﻿using System.Collections.Generic;

namespace SpecificationPOC.Pattern
{
    public class OrNotSpecification<T> : CompositeSpecification<T>
    {
        public ISpecification<T> Left { get; private set; }
        public ISpecification<T> Right { get; private set; }

        public OrNotSpecification(ISpecification<T> left,
                                  ISpecification<T> right)
        {
            Left = left;
            Right = right;
        }

        public override bool IsSatisfiedBy(T candidate) => Left.IsSatisfiedBy(candidate) | Right.IsSatisfiedBy(candidate) != true;

        public override List<BusinessRuleNode> GetChildrenNodes(BusinessRuleNode parentNode)
        {
            return SpecificationChainHelper.GetOperator(Left, Right, parentNode, "OrNot");
        }
    }
}
