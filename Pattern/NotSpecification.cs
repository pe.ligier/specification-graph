﻿using System.Collections.Generic;

namespace SpecificationPOC.Pattern
{
    public class NotSpecification<T> : CompositeSpecification<T>
    {
        public ISpecification<T> Other { get; private set; }

        public NotSpecification(ISpecification<T> other) => Other = other;

        public override bool IsSatisfiedBy(T candidate) => !Other.IsSatisfiedBy(candidate);

        public override List<BusinessRuleNode> GetChildrenNodes(BusinessRuleNode parentNode)
        {
            return SpecificationChainHelper.GetOperator(Other, parentNode, "Not");
        }
    }
}
