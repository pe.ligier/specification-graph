﻿using System.Collections.Generic;

namespace SpecificationPOC
{
    public class CustomerMustBeMajor : CompositeSpecification<Customer>
    {
        private const int MinimumAgeRequired = 18;

        public override bool IsSatisfiedBy(Customer candidate)
        {
            return candidate.Age >= MinimumAgeRequired;
        }

        public override List<BusinessRuleNode> GetChildrenNodes(BusinessRuleNode parentNode)
        {
            return SpecificationChainHelper.GetLeaf(parentNode, GetType().Name);
        }
    }
}
