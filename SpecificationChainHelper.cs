﻿using System;
using System.Collections.Generic;

namespace SpecificationPOC
{
    public static class SpecificationChainHelper
    {
        public static List<BusinessRuleNode> GetLeaf(BusinessRuleNode parent, string name)
        {
            var current = new BusinessRuleNode
            {
                Guid = Guid.NewGuid(),
                Name = name,
                ParentGuid = parent.Guid,
                ParentName = parent.Name
            };

            var list = new List<BusinessRuleNode> { current };
            return list;
        }

        public static List<BusinessRuleNode> GetSubSpecification<T>(this ISpecification<T> spec, BusinessRuleNode parent, string name)
        {
            var current = new BusinessRuleNode
            {
                Guid = Guid.NewGuid(),
                Name = name,
                ParentGuid = parent?.Guid,
                ParentName = parent?.Name
            };

            var childList = spec.GetChildrenNodes(current);

            var list = new List<BusinessRuleNode>(childList) { current };
            return list;
        }

        public static List<BusinessRuleNode> GetOperator<T>(ISpecification<T> leftSpec, ISpecification<T> rightSpec, BusinessRuleNode parent, string name)
        {
            var current = new BusinessRuleNode
            {
                Guid = Guid.NewGuid(),
                Name = name,
                ParentGuid = parent?.Guid,
                ParentName = parent?.Name
            };

            var childList = new List<BusinessRuleNode>();
            childList.AddRange(leftSpec.GetChildrenNodes(current));
            childList.AddRange(rightSpec.GetChildrenNodes(current));

            var list = new List<BusinessRuleNode>(childList) { current };
            return list;
        }

        public static List<BusinessRuleNode> GetOperator<T>(this ISpecification<T> otherSpec, BusinessRuleNode parent, string name)
        {
            var current = new BusinessRuleNode
            {
                Guid = Guid.NewGuid(),
                Name = name,
                ParentGuid = parent?.Guid,
                ParentName = parent?.Name
            };

            var childList = new List<BusinessRuleNode>();
            childList.AddRange(otherSpec.GetChildrenNodes(current));

            var list = new List<BusinessRuleNode>(childList) { current };
            return list;
        }
    }
}
