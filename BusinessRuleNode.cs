﻿using System;

namespace SpecificationPOC
{
    public class BusinessRuleNode
    {
        public Guid? ParentGuid { get; set; }
        public string ParentName { get; set; }
        public Guid? Guid { get; set; }
        public string Name { get; set; }
    }
}
