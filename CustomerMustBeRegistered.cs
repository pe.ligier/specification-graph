﻿using System.Collections.Generic;

namespace SpecificationPOC
{
    public class CustomerMustBeRegistered : CompositeSpecification<Customer>
    {
        public override bool IsSatisfiedBy(Customer candidate)
        {
            return candidate.IsRegistered;
        }

        public override List<BusinessRuleNode> GetChildrenNodes(BusinessRuleNode parentNode)
        {
            return SpecificationChainHelper.GetLeaf(parentNode, GetType().Name);
        }
    }
}
