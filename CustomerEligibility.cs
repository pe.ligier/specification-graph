﻿using System.Collections.Generic;

namespace SpecificationPOC
{
    public class CustomerEligibility : CompositeSpecification<Customer>
    {
        private readonly ISpecification<Customer> _chain;

        public CustomerEligibility()
        {
            var customerMustBeMajor = new CustomerMustBeMajor();
            var customerMustBeRegistered = new CustomerMustBeRegistered();

            _chain = customerMustBeMajor.And(customerMustBeRegistered);
        }

        public override bool IsSatisfiedBy(Customer candidate)
        {
            return _chain.IsSatisfiedBy(candidate);
        }

        public override List<BusinessRuleNode> GetChildrenNodes(BusinessRuleNode parentNode)
        {
            return _chain.GetSubSpecification(parentNode, GetType().Name);
        }
    }
}
